/**
 * ...
 */

import React, { useState, useEffect } from 'react';
import {
  Tabs,
  Tab,
  Grid,
  Button,
  Box,
  Paper,
} from '@mui/material';
import { StyledBox, TextFieldStyled } from './styled';
import SubCampaigns from './SubCampaigns';
import { Campaign, initialCampaign } from '../Models';


const Campaigns = () => {
  const [isSubmit, setIsSubmit] = useState(false);
  const [activeTab, setActiveTab] = useState(0);
  const [campaign, setCampaign] = useState<Campaign>(initialCampaign);
  const [errors, setErrors] = useState<{ [key: string]: boolean }>({});
  const handleTabChange = (event: React.ChangeEvent<{}>, newTab: number) => {
    setActiveTab(newTab);
  };
  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setCampaign((prevCampaign) => ({
      ...prevCampaign,
      information: {
        ...prevCampaign.information,
        [name]: value,
      },
    }));
  };
  const handleAddSubCampaign = () => {
    setCampaign((prevCampaign) => {
      const subCampaigns = [...prevCampaign.subCampaigns];
      subCampaigns.push({
        name: `Chiến dịch con ${subCampaigns.length + 1}`,
        status: true,
        ads: [
          {
            name: 'Quảng cáo 1',
            quantity: 0,
          },
        ],
      });
      return {
        ...prevCampaign,
        subCampaigns,
      };
    });
  };
  const handleAddAd = (subCampaignIndex: number) => {
    setCampaign((prevCampaign) => {
      const subCampaigns = [...prevCampaign.subCampaigns];
      subCampaigns[subCampaignIndex].ads.push({
        name: `Quảng cáo ${subCampaigns[subCampaignIndex].ads.length + 1}`,
        quantity: 0,
      });
      return {
        ...prevCampaign,
        subCampaigns,
      };
    });
  };
  const handleSubCampaignNameChange = (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>, index: number) => {
    const { value } = event.target;
    setCampaign((prevCampaign) => {
      const subCampaigns = [...prevCampaign.subCampaigns];
      subCampaigns[index].name = value;
      return {
        ...prevCampaign,
        subCampaigns,
      };
    });
  };
  const handleAdNameChange = (
    event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>,
    subCampaignIndex: number,
    adIndex: number
  ) => {
    const { value } = event.target;
    setCampaign((prevCampaign) => {
      const subCampaigns = [...prevCampaign.subCampaigns];
      subCampaigns[subCampaignIndex].ads[adIndex].name = value;
      return {
        ...prevCampaign,
        subCampaigns,
      };
    });
  };
  const handleAdQuantityChange = (
    event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>,
    subCampaignIndex: number,
    adIndex: number
  ) => {
    const { value } = event.target;
    setCampaign((prevCampaign) => {
      const subCampaigns = [...prevCampaign.subCampaigns];
      subCampaigns[subCampaignIndex].ads[adIndex].quantity = parseInt(value) || 0;
      return {
        ...prevCampaign,
        subCampaigns,
      };
    });
  };
  const handleSubCampaignStatusChange = (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>, index: number) => {
    const { checked } = event.target as HTMLInputElement;
    setCampaign((prevCampaign) => {
      const subCampaigns = [...prevCampaign.subCampaigns];
      subCampaigns[index].status = checked;
      return {
        ...prevCampaign,
        subCampaigns,
      };
    });
  };
  const handleDeleteAds = (subCampaignIndex: number, adIndexes: number[]) => {
    setCampaign((prevCampaign) => {
      const updatedCampaign: Campaign = { ...prevCampaign };
      if (
        subCampaignIndex >= 0 &&
        subCampaignIndex < updatedCampaign.subCampaigns.length
      ) {
        const subCampaign = updatedCampaign.subCampaigns[subCampaignIndex];
        const validAdIndexes = adIndexes.filter(
          (adIndex) =>
            adIndex >= 0 && adIndex < subCampaign.ads.length
        );
        validAdIndexes.reverse().forEach((adIndex) => {
          subCampaign.ads.splice(adIndex, 1);
        });
      }

      return updatedCampaign;
    });
  };

  const handleSubmit = () => {
    setIsSubmit(true);
    const error = checkError();
    // Check if there are any errors
    if (Object.keys(error).length === 0) {
      alert('thêm thành công chiến dịch ' + JSON.stringify(campaign));
    } else {
      alert('Vui lòng điền đúng và đầy đủ thông tin');
    }
  };
  const checkError = () => {
    const newErrors: { [key: string]: boolean } = {};
    if (campaign.information.name === '') {
      newErrors['information.name'] = true;
    }
    let count = 0;
    campaign.subCampaigns.forEach((subCampaign, subCampaignIndex) => {
      if (subCampaign.name === '') {
        newErrors[`subCampaigns[${subCampaignIndex}].name`] = true;
      }
      subCampaign.ads.forEach((ad, adIndex) => {
        if (ad.name === '') {
          count++;
          newErrors[`subCampaigns[${subCampaignIndex}].ads[${adIndex}].name`] = true;
        }
        if (ad.quantity <= 0) {
          count++;
          newErrors[`subCampaigns[${subCampaignIndex}].ads[${adIndex}].quantity`] = true;
        }
      });
      if (count > 0) {
        newErrors[`subCampaigns[${subCampaignIndex}]`] = true;
      }
      count = 0;
    })
    setErrors(newErrors);
    return newErrors;
  }
  useEffect(() => {
    if (isSubmit) {
      checkError();
    }
  }, [campaign]);
  return (
    <>
      <Grid container>
        <Grid item xs={12} sx={{ mt: 3, borderBottom: '1px solid grey' }}>
          <StyledBox>
            <Button onClick={handleSubmit} variant="contained" color="primary">
              Submit
            </Button>
          </StyledBox>
        </Grid>
      </Grid>
      <Grid sx={{ p: 3 }}>
        <Grid item xs={12}>
          <Paper variant="outlined" sx={{ borderRadius: '8px' }} square={false}>
            <Tabs value={activeTab} onChange={handleTabChange}>
              <Tab label="Thông tin" />
              <Tab label="Chiến dịch con" />
            </Tabs>
            {
              activeTab === 0 ? (
                <Box p={2}>
                  <Grid container className="spacing-xs-2">
                    <Grid item xs={12}>
                      <TextFieldStyled
                        sx={{
                          m: 1,
                        }}
                        fullWidth
                        label="Tên chiến dịch *"
                        name="name"
                        type="text"
                        variant="standard"
                        value={campaign.information.name}
                        onChange={handleInputChange}
                        error={errors['information.name']}
                        helperText={errors['information.name'] ? 'Dữ liệu không hợp lệ' : ''}
                      />
                      <TextFieldStyled
                        sx={{ m: 1 }}
                        fullWidth
                        label="Mô tả"
                        name="describe"
                        type="text"
                        variant="standard"
                        value={campaign.information.describe}
                        onChange={handleInputChange}
                      />

                    </Grid>
                  </Grid>
                </Box>
              )
                : <SubCampaigns errors={errors} campaign={campaign} handleAddSubCampaign={handleAddSubCampaign} handleAddAd={handleAddAd} handleAdNameChange={handleAdNameChange} handleSubCampaignNameChange={handleSubCampaignNameChange}
                  handleSubCampaignStatusChange={handleSubCampaignStatusChange} handleAdQuantityChange={handleAdQuantityChange} handleDeleteAds={handleDeleteAds} />
            }
          </Paper>
        </Grid>
      </Grid>

    </>
  );
};

export default Campaigns;
