/**
 * ...
 */

import React, { useState } from 'react';
import {
    Grid,
    Box,
    CardContent,
    CardHeader,
    IconButton,
    FormGroup,
    FormControlLabel,
    Checkbox,
    Tooltip
} from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import { pink } from '@mui/material/colors';
import { CardStyled, IconCheckedStyled, TitleCardStyled, TextFieldStyled } from '../styled';
import ListAds from './ListAds';
import { Campaign, Ad, SubCampaign } from '../../Models';
interface SubCampaignProps {
    campaign: Campaign;
    handleAddSubCampaign: () => void;
    handleAddAd: (index: number) => void;
    handleSubCampaignNameChange: (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>, index: number) => void;
    handleSubCampaignStatusChange: (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>, index: number) => void;
    handleAdNameChange: (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>, subCampaignIndex: number, adIndex: number) => void;
    handleAdQuantityChange: (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>, subCampaignIndex: number, adIndex: number) => void;
    handleDeleteAds: (subCampaignIndex: number, adIndexes: number[]) => void;
    errors: { [key: string]: boolean };
}

const SubCampaigns = (props: SubCampaignProps) => {
    const { errors, campaign, handleAddSubCampaign, handleAddAd, handleAdNameChange, handleSubCampaignNameChange, handleSubCampaignStatusChange, handleAdQuantityChange, handleDeleteAds } = props;
    const [activeSubCampaign, setActiveSubCampaign] = useState(0);
    const handleChangeActiveSubCampaign = (subCampaignIndex: number, subCampaign: SubCampaign) => {
        setActiveSubCampaign(subCampaignIndex);
    }
    return (
        <>
            <Box p={2}>
                <Grid container>
                    <Grid item xs={12}>
                        <div style={{ display: 'flex' }}>
                            <div onClick={handleAddSubCampaign}>
                                <IconButton
                                    color="secondary"
                                    style={{ backgroundColor: 'rgb(237, 237, 237)' }}
                                >
                                    <AddIcon sx={{ color: pink[500] }} />
                                </IconButton>
                            </div>
                            {campaign.subCampaigns.map((subCampaign, subCampaignIndex) => {
                                const totalAdsQuantity: number = subCampaign.ads.reduce(
                                    (total: number, ad: Ad) => {
                                        if (!isNaN(ad.quantity)) {
                                            return total + ad.quantity;
                                        }
                                        return total;
                                    },
                                    0
                                );
                                return (

                                    <CardStyled active={subCampaignIndex === activeSubCampaign} key={subCampaignIndex} onClick={() => handleChangeActiveSubCampaign(subCampaignIndex, subCampaign)}>
                                        <CardHeader
                                            sx={{ padding: '8px 8px 4px' }}
                                            title={
                                                <TitleCardStyled sx={{ color: errors[`subCampaigns[${subCampaignIndex}]`] === true ? 'red' : 'inherit' }} variant="h6" component="h6" >
                                                    {subCampaign.name}
                                                    <IconCheckedStyled fontSize="small" checked={subCampaign.status}>
                                                        <path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z" />
                                                    </IconCheckedStyled>
                                                </TitleCardStyled>
                                            }
                                        />
                                        <CardContent sx={{ padding: '0px 8px' }}>
                                            <Tooltip title="Số lượng" placement="left" arrow>
                                                <TitleCardStyled variant="h5" fontWeight="bold" sx={{ textAlign: 'center' }}>
                                                    {totalAdsQuantity}
                                                </TitleCardStyled>
                                            </Tooltip>
                                        </CardContent>
                                    </CardStyled>


                                )
                            }
                            )}
                        </div>
                    </Grid>
                    <Grid item xs={12}>
                        <Grid container sx={{ mt: 2 }}>
                            <Grid item xs={8}>
                                <TextFieldStyled
                                    sx={{ m: 1 }}
                                    fullWidth
                                    label="Tên chiến dịch *"
                                    name="name"
                                    type="text"
                                    variant="standard"
                                    value={campaign.subCampaigns[activeSubCampaign].name}
                                    error={errors[`subCampaigns[${activeSubCampaign}].name`]}
                                    helperText={errors[`subCampaigns[${activeSubCampaign}].name`] ? 'Dữ liệu không hợp lệ' : ''}
                                    onChange={(event) => handleSubCampaignNameChange(event, activeSubCampaign)}
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <FormGroup>
                                    <FormControlLabel sx={{ justifyContent: 'center' }} control={<Checkbox onChange={(event) => handleSubCampaignStatusChange(event, activeSubCampaign)} checked={campaign.subCampaigns[activeSubCampaign].status} />} label="Đang hoạt động" />
                                </FormGroup>
                            </Grid>
                        </Grid>
                    </Grid>
                    <ListAds campaign={campaign} errors={errors} activeSubCampaign={activeSubCampaign} handleAddAd={handleAddAd} handleAdNameChange={handleAdNameChange} handleAdQuantityChange={handleAdQuantityChange} handleDeleteAds={handleDeleteAds} />
                </Grid>
            </Box>
        </>
    );
};

export default SubCampaigns;
