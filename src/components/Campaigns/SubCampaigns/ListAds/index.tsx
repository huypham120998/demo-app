/**
 * ...
 */

import React from 'react';
import {
    IconButton,
    Checkbox,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    Button,
    ButtonGroup,
    Tooltip
} from '@mui/material';
import AddIcon from '@mui/icons-material/Add';

import DeleteIcon from '@mui/icons-material/Delete';
import { Ad, Campaign } from '../../../Models';
import { TextFieldStyled } from '../../styled';
function createData(
    name: string,
    quantity: number,
): Ad {
    return {
        name,
        quantity,
    };
}

interface ListAdsProps {
    errors: { [key: string]: boolean };
    campaign: Campaign;
    activeSubCampaign: number;
    handleAddAd: (index: number) => void;
    handleAdNameChange: (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>, subCampaignIndex: number, adIndex: number) => void;
    handleAdQuantityChange: (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>, subCampaignIndex: number, adIndex: number) => void;
    handleDeleteAds: (subCampaignIndex: number, adIndexes: number[]) => void;
}

const ListAds = (props: ListAdsProps) => {
    const { errors, campaign, activeSubCampaign, handleAddAd, handleAdNameChange, handleAdQuantityChange, handleDeleteAds } = props;
    const rows = campaign.subCampaigns[activeSubCampaign].ads.map((ad) => createData(ad.name, ad.quantity));
    interface EnhancedTableProps {
        numSelected: number;
        onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
        rowCount: number;
    }
    const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
        if (event.target.checked) {
            const newSelected = Array.from(rows.keys());
            setSelected(newSelected);
            return;
        }
        setSelected([]);
    };




    const [selected, setSelected] = React.useState<number[]>([]);

    function EnhancedTableHead(props: EnhancedTableProps) {
        const { onSelectAllClick, numSelected, rowCount } =
            props;
        return (
            <TableHead>
                <TableRow>
                    <TableCell padding="checkbox">
                        <Checkbox
                            color="primary"
                            indeterminate={numSelected > 0 && numSelected < rowCount}
                            checked={rowCount > 0 && numSelected === rowCount}
                            onChange={onSelectAllClick}
                            inputProps={{
                                'aria-label': 'select all desserts',
                            }}
                        />
                    </TableCell>
                    <TableCell>
                        {
                            numSelected === 0 ?
                                (<p>Tên quảng cáo*</p>
                                ) : <div onClick={() => handleDeleteAds(activeSubCampaign, selected)}>
                                    <IconButton
                                        size="small"
                                    >    <Tooltip title="Xóa" placement="bottom">
                                            <DeleteIcon fontSize="small" />
                                        </Tooltip>

                                    </IconButton>
                                </div>

                        }
                    </TableCell>
                    <TableCell style={{ width: '38%' }}>
                        {
                            numSelected === 0 &&
                            <p>Số lượng*</p>
                        }
                    </TableCell>
                    <TableCell align="right" style={{ padding: '0px 16px', width: '120px' }}>
                        <ButtonGroup>
                            <Button
                                variant="outlined"
                                color="primary"
                                startIcon={<AddIcon />}
                                onClick={() => handleAddAd(activeSubCampaign)}
                            >
                                Thêm
                            </Button>
                        </ButtonGroup>
                    </TableCell>
                </TableRow>
            </TableHead>
        );
    }
    const handleClick = (event: React.MouseEvent<unknown>, index: number) => {
        const selectedIndex = selected.indexOf(index);
        let newSelected: number[] = [];
        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, index);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }
        setSelected(newSelected);
    };

    const isSelected = (id: number) => selected.indexOf(id) !== -1;
    return (
        <Table aria-label="ad table">
            <EnhancedTableHead
                numSelected={selected.length}
                onSelectAllClick={handleSelectAllClick}
                rowCount={rows.length}
            />
            <TableBody>
                {
                    rows.map((ad, index) => {
                        const isItemSelected = isSelected(index);
                        return (
                            <TableRow
                                role="checkbox"
                                aria-checked={isItemSelected}
                                tabIndex={-1}
                                key={index}
                                selected={isItemSelected}
                            >
                                <TableCell sx={{ padding: '0px 0px 0px 4px' }}>
                                    <Checkbox
                                        color="primary"
                                        checked={isItemSelected}
                                        onClick={(event) => handleClick(event, index)}
                                    />
                                </TableCell>
                                <TableCell sx={{ paddingTop: '8px', paddingBottom: '8px' }}>
                                    <TextFieldStyled
                                        fullWidth
                                        id="standard-name"
                                        type="text"
                                        variant="standard"
                                        value={ad.name}
                                        onChange={(event) => handleAdNameChange(event, activeSubCampaign, index)}
                                        error={errors[`subCampaigns[${activeSubCampaign}].ads[${index}].name`]}
                                        helperText={errors[`subCampaigns[${activeSubCampaign}].ads[${index}].name`] && ''}
                                    />
                                </TableCell>
                                <TableCell style={{ paddingTop: '8px', paddingBottom: '8px', width: '50%' }}>
                                    <TextFieldStyled
                                        fullWidth
                                        id="standard-number"
                                        type="number"
                                        variant="standard"
                                        value={ad.quantity}
                                        onChange={(event) => handleAdQuantityChange(event, activeSubCampaign, index)}
                                        error={errors[`subCampaigns[${activeSubCampaign}].ads[${index}].quantity`]}
                                        helperText={errors[`subCampaigns[${activeSubCampaign}].ads[${index}].quantity`] && ''}
                                    />
                                </TableCell>
                                <TableCell align="right">

                                    <div onClick={() => !isItemSelected && handleDeleteAds(activeSubCampaign, [index])}>

                                        <IconButton
                                            size="small"
                                            disabled={isItemSelected}
                                            sx={{ cursor: isItemSelected ? 'none' : 'pointer' }}
                                        >
                                            <Tooltip title="Xóa" placement="bottom">
                                                <DeleteIcon fontSize="small" />
                                            </Tooltip>
                                        </IconButton>
                                    </div>

                                </TableCell>
                            </TableRow>
                        )
                    }
                    )}
            </TableBody>
        </Table>
    );
};

export default ListAds;
