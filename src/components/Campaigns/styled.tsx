import styled from "styled-components";
import {
  Box,
  Card,
  SvgIcon,
  Typography,
  TextField
} from '@mui/material';

interface CardProps {
  active: boolean;
}
interface IconCheckedProps {
  checked: boolean;
}

export const StyledBox = styled(Box)`
    display: flex;
    padding: 10px 20px;
    justify-content: flex-end;
`;
export const CardStyled = styled(Card)<CardProps>`
width: 210px;
height: 120px;
margin-left: 16px;
cursor: pointer;
border: 2px solid  ${(props) => props.active ? ' rgb(33, 150, 243)' : 'rgb(250, 250, 250)'};
`;
export const TitleCardStyled = styled(Typography)`
text-align: center;
white-space: normal;
word-break: break-all;
`;
export const IconCheckedStyled = styled(SvgIcon)<IconCheckedProps>`
  font-size: 14px;
  color:  ${(props) => props.checked ? 'rgb(0, 128, 0)' : 'rgb(141, 141, 141)'};
  padding-left: 8px;
`;
export const TextFieldStyled = styled(TextField)`
.MuiInput-underline.Mui-error{
  &:before {
    border-bottom: 2px solid red;
  };
  &:after {
    border-bottom: unset;
  };
}
`;