import React from 'react';
import logo from './logo.svg';
import './App.css';
import Campaigns from './components/Campaigns';

function App() {
  return (
    <>
     <Campaigns/>
    </>
  );
}

export default App;
